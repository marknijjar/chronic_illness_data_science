from datetime import datetime
from database.extensions import create_db_session


"""
This module contains all methods used for cleaning/grouping the database data
"""

SQL_NULL = "NULL"


def clean_database_age_column(db_session):
    """
    Converts all ages under 5 to None
    Converts all ages over 90 to None
    :param db_session:
    :return:
    """
    def get_age_from_year(age_in_year):
        return datetime.now().year-int(age_in_year)

    # Convert all date-years into age number (I.E: 1964 into 55)
    all_year_ages = db_session.execute("""select DISTINCT age from chronic_illness WHERE age > 1000""").fetchall()
    for dist_year in all_year_ages:
        # convert it to proper age
        proper_age = get_age_from_year(dist_year[0])
        # Update all rows where id in all_year_ages and year_age==year_age (with proper age)
        update_query = "UPDATE chronic_illness set age={} where age=={}".format(proper_age, dist_year[0])
        db_session.execute(update_query)

    all_invalid_ages_query = """select DISTINCT age from chronic_illness WHERE age > 90 OR age < 5 """
    all_invalid_ages = db_session.execute(all_invalid_ages_query).fetchall()

    all_invalid_ages = tuple(map(lambda x: x[0], all_invalid_ages))
    # Update all rows where id in all_year_ages and year_age==year_age (with proper age)
    update_query = "UPDATE chronic_illness set age={} where age IN {}".format(SQL_NULL, all_invalid_ages)
    # Update all rows where id in all_year_ages and year_age==year_age (with proper age)
    db_session.execute(update_query)

    # Save changes
    db_session.commit()


def clean_database_gender_column(db_session):
    # Convert all 'not specified', 'unknown', etc. into None values
    unspecified_genders_query = """select DISTINCT sex from chronic_illness WHERE sex!='male' and sex!='female' """
    unspecified_genders_results = db_session.execute(unspecified_genders_query).fetchall()

    unspecified_genders = tuple(map(lambda x: x[0], unspecified_genders_results))
    # Update all rows where id in all_year_ages and year_age==year_age (with proper age)
    update_query = "UPDATE chronic_illness set sex={} where sex IN {}".format(SQL_NULL, tuple(unspecified_genders))
    db_session.execute(update_query)

    db_session.commit()
    return True


if __name__ == "__main__":
    db_session = create_db_session("chronic_illness")

    # TODO: Drop Rows which aren't relevant? (trackable_type not in ('Symptom', 'Condition') ?
    # TODO: Drop rows of checkin_date & country?

    clean_database_age_column(db_session)
    clean_database_gender_column(db_session)

    # TODO: There are symptoms as conditions and vise-versa
    # shared_symptoms_and_conditions = db_session.execute("""
    # select DISTINCT trackable_name from chronic_illness where trackable_type='Symptom'
    # INTERSECT
    # select DISTINCT trackable_name from chronic_illness where trackable_type='Condition'
    # """).fetchall()

    print("Finished cleaning the database.")
