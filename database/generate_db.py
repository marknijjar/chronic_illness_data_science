import os
import pandas as pd
from database.models.ChronicIllness import Base, ChronicIllness

from database.extensions import create_db_engine, create_db_session


# def load_file(offset_path):
#     cur_dir = os.path.dirname(os.path.realpath(__file__))
#     path = cur_dir + offset_path
    # with open(path, 'rb') as in_file:
    #     temp_data = pickle.load(in_file)
    # return temp_data


def create_tables(database_name):
    # import all modules here that might define models so that
    # they will be registered properly on the metadata.  Otherwise
    # you will have to import them first before calling init_db()
    Base.metadata.create_all(create_db_engine(database_name))


def delete_tables(database_name):
    Base.metadata.drop(create_db_engine(database_name))


def populate_db(database_name, df):
    db_session = create_db_session(database_name)

    column_names = list(df.columns)
    db_session.bulk_insert_mappings(
        ChronicIllness,
        [
            dict(zip(column_names, row)) for row in df.itertuples(index=False)
        ])

    # for index, row in enumerate(df.itertuples(index=False)):
    #     data = dict(zip(column_names, row))
    #     new_ci_row = ChronicIllness(**data)
    #     db_session.add(new_ci_row)
    #
    #     if index % 10000 == 0:
    #         Save the changes into the database
            # db_session.commit()
            # print(index)

    db_session.commit()
    # print(index)

    # Close the database session
    db_session.close()


def generate_database(database_name, df):
    print("Creating database tables.")
    create_tables(database_name)
    print("Finished creating database tables")

    print("Populating tables with data")
    populate_db(database_name, df)
    print("Done populating database tables.")


if __name__ == "__main__":

    file_to_load = "fd-export.csv"
    df = pd.read_csv(file_to_load)

    # Convert 'nan' to None values in DataFrame
    df = df.where(pd.notnull(df), None)

    print("Creating database!")
    database_name = "chronic_illness"
    if os.path.exists(database_name):
        print("Table already exists, deleting all entries.")
        delete_tables(database_name)

    generate_database(database_name, df)
    print("Finished!")
