from math import isnan
from sqlalchemy import TEXT, INTEGER, Column
from sqlalchemy.ext.declarative import declarative_base


Base = declarative_base()


class ChronicIllness(Base):
    __tablename__ = 'chronic_illness'
    """
    id - Row Entry Index
    user_id - Unique ID for each user
    age - Age of Patient
    sex - Sex of Patient
    country - Country
    checkin_date - Date of Check In
    trackable_id
    trackable_type
    trackable_name
    trackable_value
    """
    id = Column(INTEGER, primary_key=True, autoincrement=True, unique=True)
    user_id = Column(TEXT, nullable=False)
    age = Column(INTEGER)
    sex = Column(TEXT)
    country = Column(TEXT)
    checkin_date = Column(TEXT)
    trackable_id = Column(INTEGER)
    trackable_type = Column(TEXT)
    trackable_name = Column(TEXT)
    trackable_value = Column(INTEGER)
