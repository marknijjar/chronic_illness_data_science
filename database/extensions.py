import os
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker


def create_db_engine(db_name):
    current_dir = os.path.dirname(os.path.realpath(__file__)) + "\\database_files\\"
    return create_engine("sqlite:///{}{}.db".format(current_dir, db_name), convert_unicode=True)


def create_db_session(db_name):
    engine = create_db_engine(db_name)
    Session = sessionmaker(autocommit=False,
                           autoflush=False,
                           bind=engine)
    # create a Session
    db_session = Session()
    return db_session


def shutdown_session(db_session):
    db_session.remove()
