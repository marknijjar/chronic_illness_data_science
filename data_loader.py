from database.extensions import create_db_session
import matplotlib.pyplot as plt


def plot_scatter_data(x, y, labelx, labely):
    plt.title("Number of entries Vs Number of people with that count")
    fig, ax = plt.subplots()

    all_scatters = []
    all_scatters.append(ax.scatter(x, y))#, s=s)

    plt.xlabel(labelx)
    plt.ylabel(labely)

    plt.show()


def show_clients_vs_number_of_entires(db_session, minimum_entries=10):
    """
    This method helps illistrate the number of clients and the amount of entries/data each one has submitted.
        This is helpful in determining if there is enough data to do case dependant learning.
    :param db_session:
    :param minimum_entries:
    :return:
    """
    # 1445 different amounts of user input quantities (I.E: a client having - 1 row, 5 rows, 1000 rows of entries)
    # 22071 distinct user_id's
    # 101765 Distinct trackable_id's

    # 'N' Number of Entries, Number of Users with 'N' Entries
    user_distribution_query = """with count_per_user_table as (select user_id, count(id) as count_per_user from chronic_illness GROUP BY user_id),
               dist_of_user_table as (select count_per_user, count(*) as dist_count from count_per_user_table GROUP BY count_per_user)
               select * from dist_of_user_table ORDER BY count_per_user ASC"""
    user_distribution_results = db_session.execute(user_distribution_query).fetchall()

    number_of_entries_per_client, number_of_clients = [], []
    for x, y in user_distribution_results:
        if y >= minimum_entries:
            number_of_clients.append(x)
            number_of_entries_per_client.append(y)

    plot_scatter_data(number_of_clients, number_of_entries_per_client, "Number of Clients", "Number of Entries Per Client")


def get_trackable_type_histogram(db_session):
    # 7 distinct trackable_type - ['Condition', 'Symptom', 'Weather', 'Treatment', 'Tag', 'Food', 'HBI']
    # Lets plot their ratio's
    trackable_types = ['Condition', 'Symptom', 'Weather', 'Treatment', 'Tag', 'Food', 'HBI']
    data = []

    for data_name in trackable_types:
        # 505461 Trackable_types are as 'Condition'
        results = db_session.execute("Select count(trackable_type) from chronic_illness where trackable_type=='{}'".format(data_name)).fetchone()
        data.append((data_name, results[0]))

    # Plot
    all_data = list(map(lambda x: x[1], data))
    plt.bar(trackable_types, all_data)
    plt.xticks(rotation='vertical')
    plt.title("Count of rows for trackable_type's")
    plt.show()


def test_getting_condition_from_symptoms(db_session):
    trackable_name_search = "Anger"
    angry_users = db_session.execute("""select DISTINCT user_id from chronic_illness WHERE trackable_name=="{}" """.format(trackable_name_search)).fetchall()

    angry_users = tuple(map(lambda x: x[0], angry_users))
    user_conditions = db_session.execute(
        """
        select DISTINCT user_id, age, sex, trackable_name 
        FROM chronic_illness 
        WHERE user_id IN {} AND trackable_type=="Condition" 
        """.format(angry_users)
    ).fetchall()

    return user_conditions


def plot_num_symptoms_for_conditions(db_session):
    # Get the top 25 conditions that have the most available symptoms data
    conditions_symptoms_count = db_session.execute(
        """
                 with all_symptoms as (select * from chronic_illness WHERE trackable_type=="Symptom" GROUP BY trackable_name),
                 all_conditions as (select DISTINCT trackable_name, user_id from chronic_illness WHERE trackable_type=="Condition")
    
                select count(all_symptoms.trackable_name), all_conditions.trackable_name 
                FROM all_symptoms
                JOIN all_conditions ON all_conditions.user_id=all_symptoms.user_id
                GROUP BY all_conditions.trackable_name
                ORDER BY count(all_symptoms.trackable_name) DESC
                LIMIT 25
        """
    ).fetchall()

    conditions_names, symptom_counts = list(zip(*conditions_symptoms_count))

    plt.bar(symptom_counts, conditions_names)
    plt.xticks(rotation='vertical')
    plt.title("Number of Symptoms for Top Conditions")
    plt.show()


db_session = create_db_session("chronic_illness")

show_clients_vs_number_of_entires(db_session)
get_trackable_type_histogram(db_session)
plot_num_symptoms_for_conditions(db_session)


# Other data information below:
test_getting_condition_from_symptoms(db_session)

# 62682 distinct trackable_name data entries
db_session.execute("with dist_track as (Select distinct trackable_name from chronic_illness) select count(*) from dist_track").fetchall()


# 7 distinct trackable_type - ['Condition', 'Symptom', 'Weather', 'Treatment', 'Tag', 'Food', 'HBI']
db_session.execute("with dist_track as (Select distinct trackable_type from chronic_illness) select count(*) from dist_track").fetchall()


# 8518 Distinct trackable_value types
db_session.execute("with dist_track as (Select distinct trackable_value from chronic_illness) select count(*) from dist_track").fetchall()

"""
    user_id - Unique ID for each user
    age - Age of Patient
    sex - Sex of Patient
    country - Country
    checkin_date - Date of Check In
    trackable_id - similar but not the same as user_id
    trackable_type - the type of information (Symptom, Condition, Tag, etc.)
    trackable_name - The description/example of the type
    trackable_value - Number scale?
"""


# Find how many users' 'trackable_type' have all: 'Condition', 'Treatment' and 'Symptom'
# 6845 Users with at least 1 entry in all three categories
users_with_all  = db_session.execute("""
Select user_id from chronic_illness where trackable_type=='Condition'
INTERSECT
select user_id from chronic_illness WHERE trackable_type=='Treatment'
INTERSECT
select user_id from chronic_illness WHERE trackable_type=='Symptom'
""").fetchall()


# TODO: Given 'Symptoms' (and optional [age, sex]) predict 'Condition'

# NOTE: Symptoms can be ambiguous (text) - So we must be able to categorize them.

# TODO: Look at the correlation/co-variance between the two groups (Symptom and Condition) -- Correlation Matrix

# TODO: Create a model for each Condition -- Can use probability outputs -- Not enough data?
