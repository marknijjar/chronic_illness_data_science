This repository is for analysis of the chronic illness dataset from "https://www.kaggle.com/flaredown/flaredown-autoimmune-symptom-tracker".

Instructions for usage:

1) The csv file must be placed under the 'database' folder.
2) Run the generate_db.py script. Note this will take a few minutes! It is converting every row of the CSV into a database file.
3) Optionally run clean_db_data.py to clean up the database values ('age' & 'sex' columns)
4) Run data_loader.py to see some graphs and database queries for the data set.

My concept was to create a prediction model for each available condition. Unfortunately I have currently concluded for myself that each condition does not contain enough associated data for itself to be useful in a machine learning environment. With a max of ~2500 entries for 1 condition. Additionally the 'trackable_type' of 'tag' contains a lot of information which doesn't seem to be very helpful in a machine learning approach (NLP required but the data itself doesn't lend much insight as it contains a lot of junk).